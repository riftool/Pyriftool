import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Pyriftool",
    version="0.0.1",
    author="Barad & Riftin",
    description="Riftool Python CLI",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/riftool/cli",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
