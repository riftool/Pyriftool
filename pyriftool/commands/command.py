class Command:
    def __init__(self, tool_id, priority=5, params=None):
        self.tool_id = tool_id
        self.priority = priority
        self.params = params
        self.command = self.__class__.__name__
