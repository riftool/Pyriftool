from pyriftool.commands.execute_shell_command import ExecuteShellCommand


class TaskList(ExecuteShellCommand):
    def __init__(self, *args, **kwargs):
        params = {'linux': 'ps', 'windows': 'tasklist'}
        super(TaskList, self).__init__(params=params, *args, **kwargs)
