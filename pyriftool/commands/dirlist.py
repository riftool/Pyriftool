from pyriftool.commands.execute_shell_command import ExecuteShellCommand


class Dirlist(ExecuteShellCommand):
    def __init__(self, *args, **kwargs):
        params = {'linux': 'ls', 'windows': 'dir'}
        super(Dirlist, self).__init__(params=params, *args, **kwargs)
